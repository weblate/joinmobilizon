section:
  title: Soutenir Framasoft
  lead: Parce que libre ne veut pas dire gratuit…
  text: L’association Framasoft ne vit **que** de vos dons.<br />Ce qu’on a fait vous
    a plu ? Vous pensez que nous allons dans le bon sens ? Alors, si vous en avez
    l’envie et les moyens, nous vous invitons à faire un don.
  why: Pourquoi soutenir Framasoft ?
  money: Où va votre argent ?
  numbers: Framasoft en quelques chiffres
  questions: Questions et contact
form:
  step1:
    title: 1. Je veux donner à Framasoft
    monthly: tous les mois
    oneshot: une fois
    other: 'Autre montant (ex : {n})'
    anonymous: Je veux que mon don reste anonyme
    receipt: Je souhaite recevoir un reçu fiscal
  step2:
    title: 2. Je complète mes informations
    private: Confidentialité
    private_tip: |-
      Ces informations ne sont requises que pour des raisons pratiques et légales
      afin de pouvoir vous délivrer un reçu fiscal.

      Sachez également que vous pouvez vérifier si votre don a bien été effectué
      dans la page « [Liste des dons](@:link.soutenir/stats) ».

      Bien évidemment, vos nom et prénom y sont anonymisés.
    type: Je représente
    part: un particulier
    corp: une structure
    corp_tip: 'Entreprise, association, collectivité…'
    mecenat: |-
      Si vous souhaitez devenir mécène de @:txt.soft et
      [apparaître sur cette page](@:link.soutenir/sponsors),
      [contactez-nous](@:link.contact/#soutenir).
    society: Raison sociale
    society_ex: 'ex : Free Software Fondation'
    society_email_ex: 'ex : contact@fsf.org'
    nickname: Pseudonyme
    nickname_ex: 'ex : Lady AAL'
    lastname: Nom
    lastname_ex: 'ex : Lovelace'
    firstname: Prénom
    firstname_ex: 'ex : Ada'
    email: E-mail
    email_ex: 'ex : a.lovelace@example.com'
    address1: Adresse
    address1_ex: 'ex : 12 rue de la liberté'
    address2: Complément
    address2_ex: 'ex : Bâtiment VI'
    zip: Code postal
    zip_ex: 'ex : 69007'
    city: Ville
    city_ex: 'ex : Lyon'
    country: Pays
    error_empty: Ce champ doit être renseigné.
    error_email: Vous devez saisir une adresse email valide.
  step3:
    title: 3. J’accède au paiement
    defisc: Infos sur la défiscalisation
    defisc_text: En France, grâce à la déduction d’impôts de {percent}, **votre don
      de {amount}** vous coûtera {defisc}.
    cb: Carte bancaire
    vir: Virement
    chq: Chèque
    pp: Paypal
    i_give: Je donne
    monthly: par mois
    now: maintenant
    coord: Vos coordonnées sont notées.
    email_send:
      - Un email vient de vous être envoyé à l’adresse
      - contenant un rappel des instructions ci-dessous.
    ok: Ok, j’ai compris
    modal_vir:
      by: par virement bancaire
      no_sepa: ' @:txt.soft ne propose pas de formulaire SEPA pour des prélèvements bancaires, mais vous pouvez nous faire un don par virement directement.<br /> Ainsi vous décidez de la somme, de la fréquence et vous pouvez le modifier à tout moment.'
      to_finish: ' Pour finaliser votre don, merci de noter nos coordonnées bancaires :'
      ref: ' Notez aussi cette <b>référence de paiement</b> qu’il faudra ajouter à la description du virement :'
      to_bank: ' Voyez ensuite avec votre banque comment planifier le virement. Certaines banques permettent de gérer ça en ligne.'
    modal_chq:
      by: par chèque
      to_finish: ' Pour finaliser votre don, merci de remplir votre chèque <b>à l’ordre de « Association @:txt.soft »</b> et de noter cette <b>référence de paiement au dos</b> :'
      to_post: ' Envoyez ensuite votre chèque au siège l’association à cette adresse postale :'
  ask:
    title: Questions
    stop: |-
      Les dons récurrents peuvent être stoppés à tout moment
      [sur simple demande](@:link.contact/#soutenir).
    edit: |-
      Si vous souhaitez modifier votre don récurrent,
      [contactez-nous](@:link.contact/#soutenir), nous stopperons les
      prélèvements et vous pourrez refaire un don.
    send: |-
      Les reçus fiscaux (en France uniquement) sont envoyés par courriel en
      mars/avril @:year.next (avant la déclaration d’impôt) pour les dons versés
      en @:year.current
    moral: |-
      Les rapports d’activité et financier de l’association peuvent être consultés
      depuis notre page de [présentation de l’association](@:link.soft/association)
    alter1: |-
      Pour [des raisons pratiques](@:link.contact/faq/#dons-alternatifs), nous
      n’acceptons pas les dons depuis les plateformes de micro paiement
      (Lilo, Flattr, Tipeee…) à l’exception de Liberapay
    lp: Faire un don via Liberapay
    alter2: |-
      Nous n’acceptons pas non plus les dons en crypto-monnaie (Bitcoin, Ether,
      Monero…) mais nous observons avec attention
      [la monnaie libre Ğ1](https://duniter.org/fr/comprendre/)<.
    other: |-
      Si vous avez d’autres questions, [les réponses se trouvent peut-être
      là](@:link.soutenir/#questions)…
why:
  maintitle: Pourquoi soutenir @:txt.soft ?
  numbers:
    title: '@:txt.soft en {year}'
    subtitle: Chiffres clés
    list1: |-
      - {years} ans d’existence
      - {members} membres et {employees} permanent·es
      - {projects} projets ({services} services éthiques en ligne)
    list2: |-
      - {ucontrib} contributeur·rices
      - {usupport} donateur·ices
      - {uvisits} visites par mois
    list3: |-
      - {dlibre} ressources libres dans notre annuaire
      - {dblog} articles blog
      - {events} interventions face à tous types de publics
    more: '[Source](https://framastats.org) — [En savoir plus](#questions)'
  money:
    title: Découvrez où va votre argent
    intro: |-
      Vos dons assurent notre indépendance (98 % de nos revenus en {year}).

      Parce que créer et maintenir des outils numériques éthiques demande
      principalement du temps et des talents humains, la majeure partie de notre
      budget sert à rémunérer (avec le plus de justesse et d’équité possible)
      nos salarié·es et prestataires.

      Chaque année, nos comptes sont vérifiés et validés par un commissaire aux
      comptes indépendant (nous publions [les rapports sur cette
      page](https://framasoft.org/association/)).
    list:
      - 'Ressources humaines :'
      - 'Serveurs et domaines :'
      - 'Frais de fonctionnement :'
      - 'Interventions et projets ext. :'
      - 'Communication :'
      - 'Achats de marchandise :'
      - 'Frais bancaires :'
    outro:  (données mises à jour en janvier 2020)
  actions:
    title: Votre don en actions…
    list:
    - '**Partager en personne**<br />
      Vos dons nous permettent de participer à près de [cent manifestations chaque
      année](https://wiki.framasoft.org/evenements).
      Nous y apportons des outils physiques (Métacartes, guides [Résolu],
      Framabooks, Flyers) pour mieux aborder le monde numérique.'
    - '**Favoriser l’éducation populaire**<br />
      Framasoft s’engage à populariser l’émancipation numérique en direction du
      plus grand nombre, en contribuant à des outils concrets fait par et pour des
      personnes engagées dans l’éducation populaire.
      [[1](https://framablog.org/2019/12/14/les-metacartes-numerique-ethique-un-outil-qui-fait-envie/)],
      [[2](https://framablog.org/2020/06/27/resolu-un-pas-de-plus-dans-contributopia/)],
      [[3](https://framablog.org/2019/11/30/mon-parcours-collaboratif-presenter-des-outils-numeriques-ethiques-a-lesse/)]'
    - '**Consolider ce qui vous sert**<br />
      L’annuaire [@:txt.libre](@:link.libre), la maison d’édition [@:txt.book](@:link.book),
      le  [@:txt.blog](@:link.blog), les services [@:txt.dio](@:link.dio), les
      actions de [@:txt.cuo](@:link.cuo)… n’existent que grâce à votre soutien.
      Faites-les vivre !'
    - '**Contribuer à d’autres communautés**<br />
      Nous voulons mettre nos outils au service des personnes qui œuvrent pour une
      société de contribution.
      Framasoft navigue dans un archipel de communautés aux valeurs proches, et
      contribue à des actions communes dans l’échange et l’entraide.'
    - '**Maintenir les outils techniques**<br />
      Framasoft, c’est une cinquantaine de sites et services libres, déployés sur
      une trentaine de serveurs.
      Nos membres en assurent le maintien, le support, l’animation et la mise à jour.
      Vos dons en assurent la gratuité pour tous.'
    - '**Dessiner un nouvel horizon numérique**<br />
      Le [MOOC CHATONS](https://mooc.chatons.org), [PeerTube](@:link.joinpeertube),
      [Mobilizon](@:link.joinmobilizon)…
      Grâce à vos dons, nous imaginons et produisons des outils numériques conviviaux,
      qui s’émancipent des lois de l’économie de l’attention pour mieux respecter
      ce qui nous relie.'
