section:
  title: Support Framasoft
  lead: Because freedom doesn’t mean it’s free…
  text: Framasoft is a not-for-profit which can **only** keep running thanks
    to your donations. Do you like what we do? Do you think we’re going in the right
    direction? If so, and you’re able to make a donation, we’d very much appreciate
    it!
  why: Why support @:txt.soft?
  money: Where is your money going to?
  numbers: '@:txt.soft in a few numbers'
  questions: Questions and contact
form:
  step1:
    title: 1. I want to give to Framasoft
    monthly: Monthly recurring donation
    oneshot: One-time donation
    other: Other amount (e.g. {n})
    anonymous: I want my donation to remain anonymous
    receipt: I would like to receive a tax receipt
  step2:
    title: 2. I complete my information
    private: Confidentiality
    private_tip: |-
      This information is necessary for practical reasons and in order to issue
      a tax receipt.

      You should also know that you can check whether your donation has been
      received. in the “[donor list](@:link.soutenir/stats)” page.

      Of course, your firtname and lastname are anonymized.
    type: I represent
    part: an individual
    corp: a company
    corp_tip: 'Company, association, community…'
    mecenat: |-
      If you would like to become a sponsor of @:txt.soft and
      [appear on this page](@:link.soutenir/sponsors),
      [contact us](@:link.contact/#soutenir).
    society: Legal entity
    society_ex: e.g. Free Software Fondation
    society_email_ex: e.g. contact@fsf.org
    nickname: Nickname
    nickname_ex: e.g. Lady AAL
    lastname: Last name
    lastname_ex: e.g. Lovelace
    firstname: First name
    firstname_ex: e.g. Ada
    email: E-mail
    email_ex: e.g. a.lovelace@example.com
    address1: Address
    address1_ex: e.g. 12 rue de la liberté
    address2: Addition of address
    address2_ex: e.g. Building VI
    zip: Postal code
    zip_ex: e.g. 69007
    city: City
    city_ex: e.g. Lyon
    country: Country
    error_empty: This field must be filled in.
    error_email: You must enter a valid email address.
  step3:
    title: 3. I access the payment
    defisc: Information on tax exemption
    defisc_text: In France, thanks to the tax deduction of {percent}, **your
      donation of {amount}** will cost you only {defisc}.
    cb: Credit card
    vir: Transfer
    chq: Check
    pp: Paypal
    i_give: I give
    monthly: monthly
    now: now
    coord: Your personal details are noted.
    email_send:
      - An email has just been sent to you at the address
      - containing a reminder of the instructions below.
    ok: Ok, I get it.
    modal_vir:
      by: by bank transfer
      no_sepa: '
        @:txt.soft does not offer a SEPA form for direct debits,
        but you can make a donation by bank transfer directly to us.<br />
        So you decide the sum, the frequency and you can change it at any time.'
      to_finish: '
        To finalize your donation, please note our bank details:'
      ref: '
        Also note this <b>payment reference</b> which must be added to the description of the transfer:'
      to_bank: '
        Then discuss with your bank how to plan the transfer.
        Some banks can handle this online.'
    modal_chq:
      by: by check
      to_finish: '
        To finalize your donation, please complete your cheque
        to the order of <b>“Association @:txt.soft”</b> and
        note this <b>payment reference at the back</b>:'
      to_post: '
        Then send your cheque to the head office of the association
        to this mailing address:'
  ask:
    title: Questions
    stop: |-
      Recurring donations can be stopped anytime, just
      [ask us](@:link.contact/#soutenir).
    edit: |-
      If you want to modify your recurring donation, please
      [contact us](@:link.contact/#soutenir), we will stop the current
      one and you will be able to make a new one.
    send: |-
      Tax receipts (in France only) are sent by postal mail in March/April
      @:year.next (before tax declaration) for @:year.current donations
    moral: |-
      Financial and activity reports can be found on the
      [not-for-profit page](@:link.soft/association)
    alter1: |-
      For [practical reasons](@:link.contact/faq/#dons-alternatifs), we do not
      accept donations from micro-payment platforms (Lilo, Flattr, Tipeee…)
      except Liberapay
    lp: Make a donation via Liberapay
    alter2: |-
      We also do not accept donations in crypto-currency (Bitcoin, Ether,
      Monero…) but we carefully observe
      [the Ğ1 free currency](https://duniter.org/fr/comprendre/).
    other: |-
      If you have more questions, [you
      may find answers there](@:link.soutenir/#questions)…
why:
  maintitle: Why support @:txt.soft?
  numbers:
    title: '@:txt.soft in {year}'
    subtitle: Key metrics
    list1: |-
      - {years}
      - {members} members and {employees} employees
      - {projects} projects ({services} ethical online services)
    list2: |-
      - {ucontrib} contributors
      - {usupport} patrons
      - {uvisits} visits per mounth
    list3: |-
      - {dlibre} free resources in our directory
      - {dblog}  blog posts
      - {events} events each year to talk and reach different audiences
    more: '[Sources](https://framastats.org) — [More infos](questions)'
  money:
    title: Find our where your money goes
    intro: |-
      Your donations ensure our independence (98% of our income in {year}).

      Because creating and maintaining ethical digital tools requires
      time and human talent, the bulk of our time and talent is spent on the
      budget is used to compensate (as fairly and equitably as possible)
      our employees and service providers.

      Each year, our accounts are audited and validated by a statutory auditor.
      independent accounts (we publish [reports on this
      page](https://framasoft.org/association/)).
    list:
    - 'Employees:'
    - 'Servers and domains:'
    - 'Operating expanses:'
    - 'Events and meetings:'
    - 'Communication:'
    - 'Goods:'
    - 'Banking fees:'
    outro: (data updated in January 2020)
  actions:
    title: Thanks to your money…
    list:
    - '**Share in person**<br />
      Your donations allow us to participate in nearly [one hundred events each year].
      year](https://wiki.framasoft.org/evenements).
      We bring physical tools (Metacards, guides [Résolu],
      Framabooks, Flyers) to better approach the digital world.'
    - '**Promoting popular education**<br />
      Framasoft is committed to popularizing digital emancipation towards the
      by contributing to concrete tools made by and for a greater number of people, by contributing to concrete tools made by and for
      people involved in popular education.
      [[1](https://framablog.org/2019/12/14/les-metacartes-numerique-ethique-un-outil-qui-fait-envie/)],
      [[2](https://framablog.org/2020/06/27/resolu-un-pas-de-plus-dans-contributopia/)],
      [[3](https://framablog.org/2019/11/30/mon-parcours-collaboratif-presenter-des-outils-numeriques-ethiques-a-lesse/)]'
    - '**Consolidate what serves you**<br />
      The directory [@:txt.libre](@:link.libre), the publishing house [@:txt.book](@:link.book),
      the [@:txt.blog](@:link.blog), the [@:txt.dio](@:link.dio) services, the
      actions of [@:txt.cuo](@:link.cuo)... exist only thanks to your support.
      Bring them to life!'
    - '**Contribute to other communities**<br />
      We want to put our tools at the service of people who work for a
      contribution company.
      Framasoft navigates in an archipelago of communities with similar values, and
      contributes to common actions in exchange and mutual aid.'
    - '**Maintain the technical tools**<br />
      Framasoft is about fifty open source sites and services, deployed on
      about thirty servers.
      Our members ensure the maintenance, the support, the animation and the update.
      Your donations ensure that it is free of charge for everyone.'
    - '**Drawing a new digital horizon**<br />
      The [MOOC CHATONS](https://mooc.chatons.org), [PeerTube](@:link.joinpeertube),
      [Mobilizon](@:link.joinmobilizon)…
      Thanks to your donations, we design and produce user-friendly digital tools,
      that emancipate themselves from the laws of the economy of attention to better respect
      what connects us.'